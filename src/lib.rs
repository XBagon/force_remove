use std::fs;
use std::path::Path;
use std::io;

///Allows removal of read-only files, other than that behaves like [`std::fs::remove_file`].
pub fn force_remove_file<P: AsRef<Path>>(path: P) -> io::Result<()> {
    set_readonly(&path,false).unwrap();
    fs::remove_file(&path)
}

///Allows removal of empty read-only directories, other than that behaves like [`std::fs::remove_dir`].
pub fn force_remove_dir<P: AsRef<Path>>(path: P) -> io::Result<()> {
    set_readonly(&path, false).unwrap();
    fs::remove_dir(&path)
}

///Allows removal of readonly directories and their read-only content, uses [`force_remove_dir`] and [`force_remove_file`].
pub fn force_remove_dir_all<P: AsRef<Path>>(path: P) -> io::Result<()> {
    for child in fs::read_dir(&path)?{
        let child = child?;
        let metadata = child.metadata()?;
        let path = child.path();
        if metadata.is_dir(){
            force_remove_dir_all(&path)?;
        }else if metadata.is_file(){
            force_remove_file(&path)?;
        }
    }
    force_remove_dir(&path)
}

fn set_readonly<P: AsRef<Path>>(path: P, readonly : bool) -> io::Result<()> {
    let mut permissions = fs::metadata(&path)?.permissions();
    permissions.set_readonly(readonly);
    fs::set_permissions(&path,permissions)
}



#[cfg(test)]
mod tests {
    use std::os;
    use std::fs;
    use std::path::Path;
    use std::fs::File;
    use std::io;

    #[test]
    fn test_force_remove_file() {
        let path = Path::new("single_file");
        File::create(&path).unwrap();
        assert!(Path::exists(&path));
        super::set_readonly(&path, true).unwrap();
        super::force_remove_file(path).unwrap();
        assert!(!Path::exists(&path));
    }

    #[test]
    fn test_force_remove_directory() {
        let path = Path::new("single_directory");
        fs::create_dir(&path).unwrap();
        assert!(Path::exists(&path));
        super::set_readonly(&path, true).unwrap();
        super::force_remove_dir(path).unwrap();
        assert!(!Path::exists(&path));
    }

    #[test]
    fn test_force_remove_directory_all() {
        let directory = Path::new("directory");
        let sub_directory = directory.join(Path::new("sub_directory"));
        let file = directory.join(Path::new("file"));
        let sub_directory_file = sub_directory.join(Path::new("sub_directory_file"));

        fs::create_dir(&directory).unwrap();
        fs::create_dir(&sub_directory).unwrap();
        File::create(&file).unwrap();
        File::create(&sub_directory_file).unwrap();


        assert!(Path::exists(&directory));
        assert!(Path::exists(&sub_directory));
        assert!(Path::exists(&file));
        assert!(Path::exists(&sub_directory_file));

        super::set_readonly(&directory, true).unwrap();
        super::set_readonly(&sub_directory, true).unwrap();
        super::set_readonly(&file, true).unwrap();
        super::set_readonly(&sub_directory_file, true).unwrap();

        super::force_remove_dir_all(&directory).unwrap();

        assert!(!Path::exists(&directory));
        assert!(!Path::exists(&sub_directory));
        assert!(!Path::exists(&file));
        assert!(!Path::exists(&sub_directory_file));
    }

    ///Currently set to unix-only, cause Windows needs Administrator privileges to create symlinks.
    #[cfg(target_os = "unix")]
    fn test_force_remove_directory_all_with_symlink() {
        let directory = Path::new("directory");
        let sub_directory = directory.join(Path::new("sub_directory"));
        let file = directory.join(Path::new("file"));
        let sub_directory_file = sub_directory.join(Path::new("sub_directory_file"));
        let symlink = directory.join(Path::new("symlink"));
        let linked_directory = Path::new("linked_directory");

        fs::create_dir(&directory).unwrap();
        fs::create_dir(&sub_directory).unwrap();
        File::create(&file).unwrap();
        File::create(&sub_directory_file).unwrap();
        make_symlink_dir(&symlink, &linked_directory).unwrap();
        fs::create_dir(&linked_directory).unwrap();


        assert!(Path::exists(&directory));
        assert!(Path::exists(&sub_directory));
        assert!(Path::exists(&file));
        assert!(Path::exists(&sub_directory_file));
        assert!(Path::exists(&symlink));
        assert!(Path::exists(&linked_directory));

        super::set_readonly(&directory, true).unwrap();
        super::set_readonly(&sub_directory, true).unwrap();
        super::set_readonly(&file, true).unwrap();
        super::set_readonly(&sub_directory_file, true).unwrap();
        super::set_readonly(&symlink, true).unwrap();
        super::set_readonly(&linked_directory, true).unwrap();

        super::force_remove_dir_all(&directory).unwrap();

        assert!(!Path::exists(&directory));
        assert!(!Path::exists(&sub_directory));
        assert!(!Path::exists(&file));
        assert!(!Path::exists(&sub_directory_file));
        assert!(!Path::exists(&symlink));
        assert!(Path::exists(&linked_directory));

        super::force_remove_dir(&linked_directory);
    }

    #[cfg(target_os = "windows")]
    fn make_symlink_dir<P: AsRef<Path>, Q: AsRef<Path>>(src: P, dst: Q) -> io::Result<()>{
        os::windows::fs::symlink_dir(src, dst)
    }

    #[cfg(target_os = "unix")]
    fn make_symlink_dir<P: AsRef<Path>, Q: AsRef<Path>>(src: P, dst: Q) -> io::Result<()>{
        os::unix::fs::symlink(src, dst)
    }
}